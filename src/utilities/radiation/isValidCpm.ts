export const isValidCpm = (cpm: number | null) => {
  return !isNaN(cpm as number) && typeof cpm === 'number' && cpm >= 0;
};
