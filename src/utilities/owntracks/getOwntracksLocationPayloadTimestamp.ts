import moment, { Moment } from 'moment';
import { OwntracksLocationPayload } from '../../types';

export const getOwntracksLocationPayloadTimestamp = (
  payload: OwntracksLocationPayload
): Moment => {
  return moment.utc((payload.created_at ?? payload.tst) * 1000);
};
