import { OwntracksLocationPayload } from '../../types';

export const getOwntracksLocationPayloadIds = (
  payload: OwntracksLocationPayload
): { userId: string; deviceId: string } => {
  const topicTokens = payload.topic.split('/');

  if (topicTokens.length !== 3 || topicTokens[0] !== 'owntracks') {
    throw new Error('OwnTracks topic invalid format');
  }

  return {
    userId: topicTokens[1],
    deviceId: topicTokens[2],
  };
};
