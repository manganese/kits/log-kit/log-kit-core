import { MessageType } from '.';

export interface MessageBase {
  type: MessageType;
}
