import { HealthMessage, OwntracksMessage, RadiationMessage } from '..';

export type Message = OwntracksMessage | RadiationMessage | HealthMessage;
