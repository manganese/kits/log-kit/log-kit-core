export enum MessageType {
  Owntracks = 'owntracks',
  Radiation = 'radiation',
  Health = 'health',
}
