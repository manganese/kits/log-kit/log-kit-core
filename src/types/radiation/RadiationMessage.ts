import { MessageBase, MessageType, RadiationPayload } from '..';

export interface RadiationMessage extends MessageBase {
  type: MessageType.Radiation;
  payload: RadiationPayload;
}
