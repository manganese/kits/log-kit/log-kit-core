export interface RadiationPayload {
  AID: string;
  GID: string;
  CPM: number;
}
