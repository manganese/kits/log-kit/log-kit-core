import { HealthMetricPayload, HealthWorkoutPayload } from '.';

export interface HealthPayload {
  metrics: HealthMetricPayload[];
  workouts: HealthWorkoutPayload[];
}
