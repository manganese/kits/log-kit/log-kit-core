import { MessageType, HealthPayload } from '..';

export interface HealthMessage {
  type: MessageType.Health;
  payload: HealthPayload;
}
