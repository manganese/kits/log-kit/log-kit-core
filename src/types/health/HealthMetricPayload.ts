export interface HealthMetricData {
  date: string;
  qty: number;
}

export interface HealthMetricPayload {
  name: string;
  units: string;
  data: HealthMetricData[];
}
