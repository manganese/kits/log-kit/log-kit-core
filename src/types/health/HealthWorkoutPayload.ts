export interface HealthWorkoutRouteData {
  // Timestamp
  timestamp: string;

  latitude: number;
  longitude: number;
  horizontalAccuracy: number;

  altitude: number;
  verticalAccuracy: number;

  course: number;
  courseAccuracy: number;

  speed: number;
  speedAccuracy: number;
}

export interface HealthWorkoutHeartRateData {
  // Timestamp
  date: string;

  // Device or list of devices separated by '|'
  source: string;

  qty: number;
  units: 'count';
}

export interface HealthWorkoutStepCountData {
  // Timestamp
  date: string;

  // Device or list of devices separated by '|'
  source: string;

  qty: number;
  units: 'steps';
}

export interface HealthWorkoutWalkingAndRunningDistanceData {
  // Timestamp
  date: string;

  // Device or list of devices separated by '|'
  source: string;

  qty: number;
  units: string;
}

export interface HealthWorkoutHeartRateRecoveryData {
  // Timestamp
  date: string;

  // Device or list of devices separated by '|'
  source: string;

  units: 'bpm';
}

export interface HealthWorkoutActiveEnergyData {
  // Timestamp
  date: string;

  // Device or list of devices separated by '|'
  source: string;

  qty: number;
  units: string;
}

export interface HealthWorkoutPayload {
  id: string;

  // Workout type e.g. 'Outdoor Run'
  name: string;

  // Timestamp
  start: string;

  // Timestamp
  end: string;

  // Duration in seconds
  duration: number;

  activeEnergyBurned: {
    qty: number;
    units: string; // 'kcal' or 'kJ'
  };

  activeEnergy: HealthWorkoutActiveEnergyData[];

  walkingAndRunningDistance: HealthWorkoutWalkingAndRunningDistanceData[];

  stepCount: HealthWorkoutStepCountData[];

  intensity: {
    qty: number;
    units: string;
  };

  heartRateData: HealthWorkoutHeartRateData[];

  heartRateRecovery: HealthWorkoutHeartRateRecoveryData[];

  // Example 'Outdoor'
  location: string;

  route: HealthWorkoutRouteData[];

  distance: {
    qty: number;
    units: string;
  };

  temperature: {
    qty: number;
    units: string; // 'degF' or 'degC'
  };

  humidity: {
    qty: number;
    units: '%';
  };

  elevationUp: {
    qty: number;
    units: string;
  };

  metadata: object;
}
