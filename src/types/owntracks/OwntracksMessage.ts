import { MessageBase, MessageType } from '..';
import { OwntracksPayload } from '.';

export interface OwntracksMessage extends MessageBase {
  type: MessageType.Owntracks;
  payload: OwntracksPayload;
}
