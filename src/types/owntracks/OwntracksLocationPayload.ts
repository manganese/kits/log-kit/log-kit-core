import { OwntracksPayloadType } from '.';

export interface OwntracksLocationPayload {
  _type: OwntracksPayloadType.Location;

  // Accuracy of the reported location in meters
  acc?: number;

  // Altitude measured above sea level in meters
  alt?: number;

  // Device battery level
  batt?: number;

  // Battery status
  // 0 = unknown
  // 1 = unplugged
  // 2 = charging
  // 3 = full
  bs: 0 | 1 | 2 | 3;

  // Course over ground
  cog?: number;

  // Latitude
  lat: number;

  // Longitude
  lon: number;

  // Radius around the region when entering/leaving
  rad?: number;

  // Trigger for the location report
  // p = ping issued randomly by background task
  // c = circular region enter/leave event
  // b = beacon region enter/leave event
  // r = response to a reportLocation cmd message
  // u = manual publish requested by the user
  // t = timer based publish in move move
  // v = updated by Settings/Privacy/Locations Services/System Services/Frequent Locations monitoring
  t?: 'p' | 'c' | 'b' | 'r' | 'u' | 't' | 'v';

  // Tracker ID used to display the initials of a user
  tid: string;

  // UNIX epoch timestamp in seconds of the location fix
  tst: number;

  // Vertical accuracy of the alt element in meters
  vac?: number;

  // Velocity
  vel?: number;

  // Barometric pressure
  p?: number;

  // Point of interest name
  poi?: string;

  // Internet connectivity status
  // w = connected to a WiFi connection
  // o = offline
  // m = mobile data
  conn?: 'w' | 'o' | 'm';

  // Name of the tag
  tag?: string;

  // Contains the original publish topic
  topic: string;

  // Contains a list of regions the device is currently in
  inregions?: string[];

  // Contains a list of region IDs the device is currently in
  inrids?: string[];

  // Name of the WLAN
  SSID?: string;

  // Wireless access point identifier
  BSSID?: string;

  // The time at which the message is constructed
  created_at: number;

  // Monitoring mode at which the message is constructed
  // 1 = significant
  // 2 = move
  m?: 1 | 2;
}
