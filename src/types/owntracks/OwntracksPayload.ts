import { OwntracksLocationPayload, OwntracksPayloadType } from '.';

export type OwntracksPayload =
  | OwntracksLocationPayload
  | {
      _type: OwntracksPayloadType;
    };
