export enum OwntracksPayloadType {
  Location = 'location',
  Waypoint = 'waypoint',
  Waypoints = 'waypoints',
  Transition = 'transition',
  Configuration = 'configuration',
  Beacon = 'beacon',
  Steps = 'steps',
  Card = 'card',
}
